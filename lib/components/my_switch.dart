library custom_switch;

import 'package:coders_test/shared/size_config.dart';
import 'package:flutter/material.dart';

class MySwitch extends StatefulWidget {
  final bool value;
  final Function(bool) onChanged;

  final String textOn;
  final String textOff;
  final Color activeColor;
  final Color inActiveColor;
  const MySwitch(
      {Key key,
      this.value,
      this.onChanged,
      this.activeColor,
      this.textOn,
      this.textOff,
      this.inActiveColor})
      : super(key: key);

  @override
  _MySwitchState createState() => _MySwitchState();
}

class _MySwitchState extends State<MySwitch>
    with SingleTickerProviderStateMixin {
  Animation _circleAnimation;
  AnimationController _animationController;
  bool state;

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
            begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
            end: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
            parent: _animationController, curve: Curves.linear));
    state = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            setState(() {
              state=!state;
              widget.onChanged(state);
            });
          },
          child: Material(
            borderRadius:
                BorderRadius.circular(getProportionateScreenWidth(20)),
            elevation: 1,
            color: Colors.transparent,
            child: Container(
              width: getProportionateScreenWidth(10),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Color(0xFFF007A82),
                      width: getProportionateScreenWidth(2)),
                  borderRadius:
                      BorderRadius.circular(getProportionateScreenWidth(20)),
                  color: _circleAnimation.value == Alignment.centerLeft
                      ? widget.inActiveColor
                      : widget.activeColor),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(4)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _circleAnimation.value == Alignment.centerRight
                        ? Text(
                            widget.textOn,
                            style: TextStyle(
                                color: Color(0xFF0E5F58),
                                fontWeight: FontWeight.w900,
                                fontFamily: 'Arial',
                                fontSize: getProportionateScreenWidth(10)),
                          )
                        : Container(),
                    Align(
                      alignment: _circleAnimation.value,
                      child: Container(
                        width: getProportionateScreenWidth(21.0),
                        height: getProportionateScreenWidth(21.0),

                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Color(0xFF0E5F58)),
                      ),
                    ),
                    _circleAnimation.value == Alignment.centerLeft
                        ? Text(
                            widget.textOff,
                            style: TextStyle(
                                color: Color(0xFF0E5F58),
                                fontFamily: 'Arial',
                                fontWeight: FontWeight.w900,
                                fontSize: getProportionateScreenWidth(10)),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
