import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';
import 'package:coders_test/shared/size_config.dart';

class VideoApp extends StatefulWidget {
  final String url;

  const VideoApp({Key key, @required this.url}) : super(key: key);
  @override
  _VideoAppState createState() => _VideoAppState();
}

class _VideoAppState extends State<VideoApp> {
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.url)
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
        
      });
      _controller.setLooping(true);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          
     
          child: _controller.value.initialized
              ? AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: VideoPlayer(_controller),
                )
              : Container(),
        ),
        IconButton(
          iconSize: getProportionateScreenWidth(50),
          icon: Icon(
            _controller.value.isPlaying ? null : Icons.play_arrow,
          ),
          color: Colors.white,
          onPressed: () {
            setState(() {
              _controller.value.isPlaying
                  ? _controller.pause()
                  : _controller.play();
            });
          },
        )
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
