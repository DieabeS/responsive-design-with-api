import 'package:coders_test/network/api.dart';
import 'package:get/get.dart';

class LangController extends GetxController {
  final videoUrl = ''.obs;
  final description = ''.obs;
  final letters = List().obs;

  @override
  void onInit() {
    super.onInit();
    getData('en');
  }

  void getData(String lang) async {
    Api api = new Api();
    await api.getData(lang).then((value) {
      videoUrl(value.vidUrl);
      description(value.disc);
      letters(value.list);

      print(videoUrl);
      print(letters);
      print(description);
    });
  }
}
