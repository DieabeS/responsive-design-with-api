import 'package:coders_test/screens/splash/splash_screen.dart';
import 'package:coders_test/shared/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
if (SizeConfig.isMobilePortrait == false) {
   WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
        .then((value) => runApp(MyApp()));
  }
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              theme: ThemeData(fontFamily: 'Hacen'),
              debugShowCheckedModeBanner: false,
              home: SplashScreen(),
            );
          },
        );
      },
    );
  }
}

