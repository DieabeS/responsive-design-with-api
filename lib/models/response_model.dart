
class ResponseData{



  var list;
  var vidUrl;
  var disc;

  ResponseData({this.list,this.disc,this.vidUrl});


  factory ResponseData.fromJson(Map <String,dynamic>json ){
   return ResponseData(
     vidUrl: json['data']['video_url'],
     disc: json['data']['description'],
     list: json['data']['careers_letters']

   );
}}