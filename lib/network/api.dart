
import 'dart:convert';

import 'package:coders_test/models/response_model.dart';
import 'package:http/http.dart' as http;

class Api {
  Future<ResponseData> getData(String lang) async {
    String url = 'https://careers.codersdc.net/api/career';

    final response = await http.get(url, headers: {'lang': lang});

    if (response.statusCode == 200) {
      return ResponseData.fromJson(jsonDecode(response.body));
      
    } else
      throw Exception('Cannot load data');
  }
}
