import 'dart:ui';
import 'dart:math' as math;
import 'package:coders_test/components/my_switch.dart';
import 'package:coders_test/components/video_player.dart';
import 'package:coders_test/controllers/lang_controller.dart';
import 'package:coders_test/shared/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final LangController controller = Get.find<LangController>();
  bool position = false;
  bool tapped = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        alignment: AlignmentDirectional.center,
        children: [
          Column(
            children: [
              Expanded(
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/appbar.png'),
                          fit: BoxFit.cover)),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                        sigmaX: getProportionateScreenWidth(2),
                        sigmaY: getProportionateScreenWidth(2)),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.0),
                      ),
                      child: Center(
                        child: Image.asset(
                          'assets/images/logo.png',
                          height: getProportionateScreenWidth(25.49),
                          width: getProportionateScreenWidth(53.79),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  color: Colors.white,
                ),
              )
            ],
          ),
          SingleChildScrollView(
            child: Obx(
              () => Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(10)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: getProportionateScreenWidth(87),
                    ),
                    Material(
                      elevation: 5,
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenWidth(44)),
                      child: Container(
                        height: getProportionateScreenWidth(200),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                                getProportionateScreenWidth(44)),
                            border: Border.all(
                                color: Colors.white,
                                width: getProportionateScreenWidth(2))),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(
                              getProportionateScreenWidth(44)),
                          child: VideoApp(url: controller.videoUrl.value),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenWidth(33),
                    ),
                    Material(
                      elevation: 5,
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenWidth(23)),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            tapped = !tapped;
                          });
                        },
                        child: Container(
                          height: getProportionateScreenWidth(50),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.bottomLeft,
                                end: Alignment.topCenter,
                                colors: [Color(0xFF386545), Color(0xFF6FC989)]),
                            borderRadius: BorderRadius.circular(
                                getProportionateScreenWidth(23)),
                          ),
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                position == false
                                    ? SizedBox()
                                    : Transform.rotate(
                                        angle: tapped == false
                                            ? 0 * math.pi / 180
                                            : 180 * math.pi / 180,
                                        child: Icon(
                                          Icons.arrow_drop_down_circle_outlined,
                                          color: Colors.white,
                                          size: getProportionateScreenWidth(
                                              12.75),
                                        ),
                                      ),
                                SizedBox(
                                  width: getProportionateScreenWidth(8),
                                ),
                                Text(
                                  position == false
                                      ? 'Description'
                                      : 'شرح التطبيق',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Arial',
                                      fontSize:
                                          getProportionateScreenWidth(18)),
                                ),
                                SizedBox(
                                  width: getProportionateScreenWidth(8),
                                ),
                                position == false
                                    ? Transform.rotate(
                                        angle: tapped == false
                                            ? 0 * math.pi / 180
                                            : 180 * math.pi / 180,
                                        child: Icon(
                                          Icons.arrow_drop_down_circle_outlined,
                                          color: Colors.white,
                                          size: getProportionateScreenWidth(
                                              12.75),
                                        ),
                                      )
                                    : SizedBox()
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    tapped == false
                        ? TweenAnimationBuilder(
                            tween: Tween<double>(begin: 1, end: 0),
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeIn,
                            builder: (context, _, child) {
                              return Container(
                                height: position == false
                                    ? _ * getProportionateScreenWidth(55)
                                    : _ * getProportionateScreenWidth(200),
                                child: Opacity(
                                  opacity: _,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: _ *
                                            getProportionateScreenWidth(25)),
                                    child: Text(
                                      controller.description.value,
                                      style: TextStyle(
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            })
                        : TweenAnimationBuilder(
                            tween: Tween<double>(begin: 0, end: 1),
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeIn,
                            builder: (context, _, child) {
                              return Container(
                                height: position == false
                                    ? _ * getProportionateScreenWidth(55)
                                    : _ * getProportionateScreenWidth(200),
                                child: Opacity(
                                  opacity: _,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: _ *
                                            getProportionateScreenWidth(25)),
                                    child: Text(
                                      controller.description.value,
                                      style: TextStyle(
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }),
                    SizedBox(
                      height: getProportionateScreenWidth(29),
                    ),
                    Container(
                      height: getProportionateScreenWidth(275),
                      child: GridView.count(
                        padding: EdgeInsets.only(top: 0),
                        physics: BouncingScrollPhysics(),
                        childAspectRatio: getProportionateScreenWidth(159) /
                            getProportionateScreenWidth(115),
                        crossAxisCount: 2,
                        crossAxisSpacing: getProportionateScreenWidth(28.5),
                        mainAxisSpacing: getProportionateScreenWidth(14.3),
                        children: controller.letters
                            .asMap()
                            .map(
                              (i, e) {
                                return MapEntry(
                                  i,
                                  Material(
                                    elevation: 3,
                                    borderRadius: BorderRadius.circular(
                                        getProportionateScreenWidth(33)),
                                    child: Container(
                                      height: getProportionateScreenWidth(115),
                                      width: getProportionateScreenWidth(159),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            getProportionateScreenWidth(33)),
                                      ),
                                      child: Center(
                                        child: Text(
                                          e,
                                          style: TextStyle(
                                            fontSize:
                                                getProportionateScreenWidth(54),
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Arial',
                                            color: i % 4 == 0
                                                ? Color(0xff6FC989)
                                                : i % 4 == 1
                                                    ? Color(0xff0E5F58)
                                                    : i % 4 == 2
                                                        ? Color(0xff007A82)
                                                        : Color(0xff00A49A),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            )
                            .values
                            .toList(),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenWidth(29),
                    ),
                    Material(
                      elevation: 5,
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenWidth(23)),
                      child: Container(
                        height: getProportionateScreenWidth(50),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                              getProportionateScreenWidth(23)),
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              position == false
                                  ? SizedBox()
                                  : Transform.rotate(
                                      angle: 90 * math.pi / 180,
                                      child: Icon(
                                        Icons.arrow_drop_down_circle_outlined,
                                        color: Color(0xFf007A82),
                                        size: getProportionateScreenWidth(17),
                                      ),
                                    ),
                              SizedBox(
                                width: getProportionateScreenWidth(8),
                              ),
                              Text(
                                position == false
                                    ? 'View More'
                                    : 'مشاهدة المزيد',
                                style: TextStyle(
                                    color: Color(0xFf007A82),
                                    fontSize: getProportionateScreenWidth(18),fontWeight: FontWeight.bold,fontFamily: 'Arial'),
                              ),
                              SizedBox(
                                width: getProportionateScreenWidth(8),
                              ),
                              position == false
                                  ? Transform.rotate(
                                      angle: -90 * math.pi / 180,
                                      child: Icon(
                                        Icons.arrow_drop_down_circle_outlined,
                                        color: Color(0xFf007A82),
                                        size: getProportionateScreenWidth(17),
                                      ),
                                    )
                                  : SizedBox()
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenWidth(32),
                    ),
                  ],
                ),
              ),
            ),
          ),
          position == false
              ? Positioned(
                  top: getProportionateScreenWidth(45),
                  left: getProportionateScreenWidth(14),
                  child: Container(
                    height: getProportionateScreenWidth(25),
                    width: getProportionateScreenWidth(46),
                    child: MySwitch(
                      activeColor: Colors.green[100],
                      inActiveColor: Colors.green[100],
                      value: false,
                      textOn: 'Ar',
                      textOff: 'En',
                      onChanged: (value) {
                        if (value)
                          controller.getData('ar');
                        else
                          controller.getData('en');
                        setState(() {
                          position = value;
                        });
                      },
                    ),
                  ),
                )
              : Positioned(
                  top: getProportionateScreenWidth(45),
                  right: getProportionateScreenWidth(14),
                  child: Container(
                    height: getProportionateScreenWidth(25),
                    width: getProportionateScreenWidth(46),
                    child: MySwitch(
                      activeColor: Colors.green[100],
                      inActiveColor: Colors.green[100],
                      value: false,
                      textOn: 'Ar',
                      textOff: 'En',
                      onChanged: (value) {
                        if (value)
                          controller.getData('ar');
                        else
                          controller.getData('en');
                        setState(() {
                          position = value;
                        });
                      },
                    ),
                  ),
                )
        ],
      ),
    );
  }
}
