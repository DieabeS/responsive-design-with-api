import 'dart:async';
import 'dart:ui';

import 'package:coders_test/controllers/lang_controller.dart';
import 'package:coders_test/screens/home/home.dart';
import 'package:coders_test/shared/size_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final LangController controller = Get.put(LangController());
  @override
  void initState() {
    super.initState();
    startTimer();
    
  }

  void startTimer() {
    Timer(Duration(seconds: 5), () {
      navigate();
    });
  }

  void navigate() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => Home()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  'assets/images/splash.png',
                ),
                fit: BoxFit.cover)),
        width: double.infinity,
        height: double.infinity,
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
            Image.asset('assets/images/logo.png',height: getProportionateScreenWidth(56),fit: BoxFit.cover,
            width: getProportionateScreenWidth(118),),
            Text(
              'عالم المهن',
              style: TextStyle(
                  fontSize: getProportionateScreenWidth(71),
                  color: Colors.white,
                  fontFamily: 'Hacen'),
            ),
          ]),
        ),
      ),
    );
  }
}
